function drawFillCircle(position, radius, color) {
    //	console.log("SC Color: " + color);
    drawFillEllipse(position, [radius, radius], 1000, 0, color);
}

function drawFillEllipse(position, scale, subdiv, angle, color) {
    //console.log("Color: " + color);
    var a = position[0];
    var b = position[1];
    var c = scale[0];
    var d = scale[1];

    var thetaStep = 2*Math.PI / subdiv; 
    
    var arr=[];
    arr.push(vec2(a,b));
    for(var i = 0; i <= 2*Math.PI; i+=thetaStep){
        var x = a + c * Math.cos(i+angle);
        var y = b + d * Math.sin(i+angle);
        arr.push(vec2(x,y));
    }
    
    var bufferId = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, bufferId );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(arr), gl.STATIC_DRAW );
    
    var myPosition = gl.getAttribLocation(shaders, "myPosition" );
    var myColor = gl.getUniformLocation(shaders, "myColor");
    gl.vertexAttribPointer( myPosition, 2, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray(myPosition);
    gl.uniform4f(myColor, color[0], color[1], color[2], color[3]);
    
    // Enter draw arrays code here
    gl.drawArrays(gl.TRIANGLE_FAN, 0, subdiv+2);
}

function drawStrokeEllipse(a, b, c, d, subdiv, sTheta, eTheta, color) {
    var thetaStep = (eTheta - sTheta) / subdiv; 
    
    var arr=[];
    for(var i = sTheta; i <= eTheta; i+=thetaStep){
        var x = a + c * Math.cos(i);
        var y = b + d * Math.sin(i);
        arr.push(vec2(x,y));
    }
    
    var bufferId = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, bufferId );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(arr), gl.STATIC_DRAW );
    
    var myPosition = gl.getAttribLocation(shaders, "myPosition" );
    var myColor = gl.getUniformLocation(shaders, "myColor");
    gl.vertexAttribPointer( myPosition, 2, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray(myPosition);
    gl.uniform4f(myColor, color[0], color[1], color[2], color[3]);
    
    // Enter draw arrays code here
    gl.drawArrays(gl.LINE_LOOP, 0, subdiv+1);
}

function drawRect(x, y, h, w, r, color){
   // console.log("Drawing Square");
    var p0 = vec2(0,0);
    var p1 = vec2(-1,0);
    var p2 = vec2(0,-1);
    var p3 = vec3(-1,-1);

    var arr = [p0, p1, p2, p3];

    var bufferId = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferId);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(arr), gl.STATIC_DRAW);

    var myPosition = gl.getAttribLocation(shaders, "myPosition");
    var myColor = gl.getUniformLocation(shaders, "myColor");
    gl.vertexAttribPointer(myPosition, 2, gl.FLOAT, false, 0,0);
    gl.uniform4f(myColor, color[0], color[1], color[2], color[3]);
    gl.enableVertexAttribArray(myPosition);

    gl.drawArrays(gl.TRIANGLE_STRIP, 0,4);
}

function drawRect(rect, color){
    //console.log("Color: " + color);
    var arr = [ vec2(rect.x,rect.y),
                vec2(rect.x+rect.width,rect.y),
                vec2(rect.x,rect.y+rect.height),
                vec2(rect.x+rect.width,rect.y+rect.height)
                ];

    var bufferId = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferId);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(arr), gl.STATIC_DRAW);

    var myPosition = gl.getAttribLocation(shaders, "myPosition");
    var myColor = gl.getUniformLocation(shaders, "myColor");
    gl.vertexAttribPointer(myPosition, 2, gl.FLOAT, false, 0,0);
    gl.uniform4f(myColor, color[0], color[1], color[2], color[3]);
    gl.enableVertexAttribArray(myPosition);

    gl.drawArrays(gl.TRIANGLE_STRIP, 0,4);
}