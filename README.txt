# Project 1 README

Developed by Yoel Ben-Avraham and Nick Kenyon

==============================================================================================================================

## HOW TO PLAY:

- Press Space to Launch the Ball
- Press A to move the Paddle left
- Press D to move the Paddle Right
- Clear all blocks to win the game!
- You have three balls with which to attempt to win!

This project is an implementation of Atari's classic game "breakout". 
Here, the player uses the W and D keys (as in the classic WASD control scheme)
to control a paddle and bounce a "ball" against "blocks". Blocks disappear when hit
with the ball, and to win the player must remove all the blocks from the scene.
Each block removed increases the player's score by one. If the ball falls off
the bottom of the screen, the player loses and the game ends. It bounces off of other
screen edges however, along with the blocks and paddle.


==============================================================================================================================

NOTES ON IMPLEMENTATION:

 To implement this project, we used javascript. We began by making a few shape "primitives" in our shapes.js
 file to allow us to draw arbitrary shapes to the screen as needed. Code for movement keys and animation
 was referenced from our previous labs, in which we had previously implemented key input and animations.
 
 The state of our game (playing vs in "menu screen") was accomplished with a simple boolean who's value
 is referenced in a conditional statement that either calls a menuAnimate() or gameAnimate() function.
 
 Of course, the animation of our game is more complex than that of the menu. For our game we have three 
 "characters" - a paddle, a ball, and a collection of blocks. The ball is drawn using a filled ellipse, while
 the paddle and blocks use filled rectangles. (The paddle uses a couple ellipses to achieve a "rounded corners" effect,
 but these are merely aesthetic - it handles collision just like any other rectangle.)
 
 Logic for the ball, paddle, and block's interactions are implemented in their respective draw functions, which are called from the
 "gameRender" function in gameScene.js. The ball makes itself bounce when hitting the paddle or the walls of the scene, but
the blocks also have code to make the ball bounce and make themselves dissappear in the event of a collision between them.
The paddle's movement is likewise done inside drawPaddle(). To detect collisions between objects, we referenced an online source
for a good algorithm for collisions between rectangles, and used that to come up with our own algorithm for detecting colissions
between ellipses and rectangles. (i.e. our ball with the paddle/blocks.)
 
 Dissappearing blocks are disabled and are thus no longer rendered are used in collision calculations (increasing performance and minimizing random lag as the game goes on)
 and incrementing a global "score" variable that is based on the number of blocks removed. 
 When the screen is cleared of blocks or the ball falls off the bottom of the screen, this number (a value between 1 and 50) is returned to the player as their final score.
 
 When the ball collides with a block or wall, it will either reverse the magnitude of it's x or y directional speed depending on which
 side it collided on. When it collides with the paddle, the part of the paddle where the ball lands directly influences its new x-velocity,
 in addition to the obvious y-velocity reversal.
 This means that if it hits the paddle nearer the edge, the ball will launch off faster and in a different direction than if it hits in the middle.
 This gives the player a lot more control than simply switching y-velocity and keeping the same x-velocity would.

 The player wins by clearing all of the blocks off of the screen. They lose if they miss hitting the ball 3 times. They can restart the game after winning or losing by pressing enter.