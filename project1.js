//Nicklas Kenyon, Yoel Ben-Avraham

var canvas;
var ctx;
var gl;
var shaders;
var shouldAnim;
var sceneID = 1; // 0 is Main Menu, 1 is Game Scene.
var scoreText;
var ballsText;
var messagesText;

function init(){
	shouldAnim = true;

	canvas = document.getElementById('gl-canvas'); // Setup the canvas

	scoreText = document.getElementById('score');
	ballsText = document.getElementById('balls');
	messagesText = document.getElementById('messages');
	scoreText.innerHTML = 'Score: ' + 0;
	ballsText.innerHTML = 'Balls Remaining: ' + ballAmmo;
	messagesText.innerHTML = 'Press Space to Launch the Ball!';

	gl = WebGLUtils.setupWebGL(canvas); // Setup the WebGL context
	if(!gl) { alert("WebGL is not avaiable");}

	gl.viewport(0,0,512,512); // Setup the viewport

	shaders = initShaders(gl, "vertex-shader", "fragment-shader");
	gl.useProgram(shaders); // Setup the shader program

	gl.clearColor(0,0,0,1); // Choose a background color - ours is black
	gl.clear(gl.COLOR_BUFFER_BIT);

	GameSetup();
	render();
}

function render(){
	gl.clear(gl.COLOR_BUFFER_BIT);

	if(sceneID == 0){
		MenuRender();
		if(shouldAnim) MenuAnimate();
	}
	else if(sceneID == 1){
		GameRender();
		if(shouldAnim) GameAnimate();
	}
	
	requestAnimFrame(render);
}

function startStopAnimation(){
	shouldAnim = !shouldAnim;
}

function handleKeydown(event){
	// Handle Main Menu Keydown
	if(sceneID == 0) MenuHandleKeydown(event);
	// Handle Game Scene Keydown
	if(sceneID == 1) GameHandleKeydown(event);
}

function handleKeyUp(event){
	// Handle Main Menu Keydown
	if(sceneID == 0) MenuHandleKeyUp(event);
	// Handle Game Scene Keydown
	if(sceneID == 1) GameHandleKeyUp(event);
}

