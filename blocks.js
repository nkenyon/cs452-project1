//blocks.js

//Our blocks are nice simple rectangles like our paddles; but exist as members of an array.

var blockRect = [];

var blockIdx = 0;

function setupBlocks(){
	for (var i = 0; i < 10; i++){
		for(var j = 0; j < 5; j++){
				//if(i <= 5){
					blockRect[blockIdx] = {x: -1.0 + (0.2*i), y: 0.8 - (0.2*j), width: 0.2, height: 0.2, active: true}; //Each individual block has its own rect.	
				//}else{
				//	blockRect[blockIdx] = {x: (0.2*i), y: 0.8 - (0.2*j), width: 0.2, height: 0.2};
				//}
				console.log("Block " + blockIdx + " initialized with:"); //Debug
				console.log(blockRect[blockIdx]);
				blockIdx++;
		}
	}
}

var blockColor = [0,0.9,0.2,1]; //Our blocks will alternate between two shades of green to help distinguish them.



function DrawBlocks(score){
	
	//console.log("Calling drawBlocks:"); //DEBUG
	//drawRect(blockRect[0], blockColor);
	
	for(var i = 0; i < blockIdx; i++){
			//console.log("Attempting to draw block " + i);
			//console.log("block's stats");
			//console.log(blockRect[i]);
			if(blockRect[i].active){
				if(i%2 == 0){
					blockColor = [0,0.6,0.2,1];
				}
				else{
					blockColor = [0,0.9,0.2,1];
				}
				drawRect(blockRect[i], blockColor);
			}
			
	}
	
	for(var i = 0; i < blockIdx; i++){
		if(blockRect[i].active && didCollide(ballCircle, blockRect[i])){ //Blocks hit by the ball "disappear" by flying offscreen
			score++;
			scoreText.innerHTML = 'Score: ' + score;
			//distances upDown and leftRight tell us what side the ball is approaching the block from, so we can appropriately calculate bounce.
			var upDown = blockRect[i].y + ((blockRect[i].height)/2) - ballCircle.y;
			var leftRight = blockRect[i].x + ((blockRect[i].width)/2) - ballCircle.x;
			
			//Bounce the ball backwards a tad here, to before it hit the block.
			ballCircle.x -= ballVelocity.x;
			ballCircle.y -= ballVelocity.y;
				
			//Now we deactivate the block
			blockRect[i].active = false;
			
			if( Math.abs(upDown) > Math.abs(leftRight) ){
				ballVelocity.y = -ballVelocity.y;
				//ballCircle.x = ballCircle.x - ballCircle.radius;
				//console.log("top-bottom collision");
			}else{
				
				ballVelocity.x = -ballVelocity.x;
				//console.log("right-left collision"); //debug
			}
			
		}
	}
	if (score >= blockRect.length){
		gameWon = true;
		messagesText.innerHTML = 'Congratulations! You Won!\n Press Enter to Start Again';
	}

	return score;
}
function resetBlocks(){
	for (var i = 0; i < blockRect.length; i++){
		blockRect[i].active = true;
	}
}
