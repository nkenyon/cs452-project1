//paddle.js
var paddleRect = {x: -.125, y: -1	, width: 0.25, height: 0.1}; //Paddle dimensions and initial location
var paddleColor = [1,0.3,0.2,1]; //Our paddle is Orange.
var paddleSpeed = 0.05;
var paddleVelocity = 0;

function DrawPaddle(){
	// Move the paddle
	paddleRect.x += paddleVelocity;
	// Clamp the paddle to the screen
	if (paddleRect.x <= -1) paddleRect.x = -1;
	if (paddleRect.x + paddleRect.width >= 1) paddleRect.x = 1-paddleRect.width;
	
	//In addition to its own rectangle, we give the paddle rounded corners for aesthetic reasons. This is done crudely, with additional circle objects and a location derived from the rectangle's own values.
	drawRect(paddleRect, paddleColor);
	drawFillCircle([paddleRect.x, paddleRect.y], paddleRect.height, paddleColor);
	drawFillCircle([paddleRect.x + paddleRect.width, paddleRect.y], paddleRect.height, paddleColor);
}

function MovePaddle(xDelta){
	paddleRect[0] += xDelta * paddleSpeed;
}