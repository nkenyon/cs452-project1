//GameScene.js
// Uses paddle.js, ball.js, and blocks.js (should precede this file in the html imports)

var score = 0;
var shouldReset = false;
var gameLost = false;
var gameWon = false;

function GameRender(){ //Draws paddle, Ball, and any existing blocks.
	DrawPaddle();
	DrawBall();
	score = DrawBlocks(score);
}

function GameAnimate(){
	
}

function GameSetup(){
	setupBlocks();
}

function GameHandleKeydown(event){
	var code = event.keyCode;

	if(code == 65) // a
		paddleVelocity = -paddleSpeed;
	else if (code == 68) // d
		paddleVelocity = paddleSpeed;
}

function GameHandleKeyUp(event){
	var code = event.keyCode;

	if(code == 65) // a
		paddleVelocity = 0;
	else if (code == 68) // d
		paddleVelocity = 0;
	else if (code == 32 && !(gameWon || gameLost)){
		launchBall();
	}
	else if (code == 13 && (gameWon || gameLost)){
		resetGame();
		console.log("Should reset game");
	}
}

// Collision Reference:
// https://developer.mozilla.org/en-US/docs/Games/Techniques/2D_collision_detection

function didCollide(rect1, rect2){
	return (rect1.x < rect2.x + rect2.width &&
		rect1.x + rect1.width > rect2.x &&
		rect1.y < rect2.y + rect2.height &&
		rect1.height + rect1.y > rect2.y);
}

function didCollide(circle, rect){ //There are only two cases where a circle intersects a rectangle: Either the circle's center is inside the rectangle, or one of the rectangle's edges in inside the circle.
	return (
			circle.x-circle.radius < rect.x + rect.width &&
			circle.x + circle.radius > rect.x &&
			circle.y-circle.radius < rect.y + rect.height &&
			circle.radius + circle.y > rect.y
			);
}

function resetGame(){
	console.log("Resetting game");
	resetBlocks();
	messagesText.innerHTML = 'Press Space to Begin';
	ballAmmo = 3;
	ballsText.innerHTML = 'Balls Remaining: ' + ballAmmo;
	gameWon = false;
	gameLost = false;
	score = 0;
	scoreText.innerHTML = 'Score: ' + score;
	isLaunched = false;
}