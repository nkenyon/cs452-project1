//ball.js

var ballCircle = {radius: 0.05, x: 0.0, y: -0.6}; //Initialize size and location
var ballVelocity = {x: 0.0, y: 0.005 };
var ballColor = [0.15,0.4,0.9,1]; //Our ball is blue
var isLaunched = false;

var ballAmmo = 3;

function DrawBall(){
	// Move the ball
	
	//If the ball is touching anything that makes it bounce, it bounces.
	if(didCollide(ballCircle, paddleRect)){ //Touching paddle
		//console.log("Ball collided with Paddle");
		
		//When the ball collides with paddle, it's new x velocity is a direct function of where it hits the paddle.
		var hitLocation = (paddleRect.x + (paddleRect.width/2)) - ballCircle.x; // more "+" on left, more "-" on right
		//This value's unsigned magnitude will range between 0.0 and 0.130 based on the hardcoded values we have for paddle and ball sizes. So we calculate new velocity as follows:
		ballVelocity.x = -hitLocation/10;
		
		ballVelocity.y = -ballVelocity.y;
		ballCircle.y = paddleRect.y + paddleRect.height + ballCircle.radius; //Prevenents ball being stuck inside paddle - yoel
	}
	else if( ballCircle.x-ballCircle.radius < -1){ //Left Wall
		//console.log("Ball collided with Left Wall");
		ballVelocity.x = -ballVelocity.x;
		
	}else if( ballCircle.x + ballCircle.radius > 1){ //Right Wall
		//console.log("Ball collided with Right Wall");
		ballVelocity.x = -ballVelocity.x;
		
	}else if( ballCircle.y + ballCircle.radius > 1){ //Top Wall
		//console.log("Ball collided with Top Wall");
		//ballVelocity.x = -ballVelocity.x;
		ballVelocity.y = -ballVelocity.y;
	}else if ( ballCircle.y + ballCircle.radius < -1){ // Player doesn't catch it with the paddle
		// Stop the ball from moving
		ballVelocity.x = 0;

		ballCircle.x = 0.0;
		ballCircle.y = -0.6;

		isLaunched = false;
		ballAmmo--;

		ballsText.innerHTML = 'Balls Remaining: ' + ballAmmo;
		messagesText.innerHTML = 'Press Space to Launch the Ball';
		if(ballAmmo<=0){
			messages.innerHTML = 'Game Over! Press Enter to Start Again!';
			ballsText.innerHTML = 'Balls Remaining: ' + 0;
			gameLost = true;
		}
	}
	
	// Update position based on status
	if(isLaunched && !(gameWon || gameLost)){
		ballCircle.x += ballVelocity.x;
		ballCircle.y += ballVelocity.y;
	}else{
		ballCircle.x = paddleRect.x + paddleRect.width / 2;
		ballCircle.y = paddleRect.y + ballCircle.radius + paddleRect.height;
		ballVelocity.x = 0;
		ballVelocity.y = 0;
	}
	//console.log("Ball color: " + ballColor);
	drawFillCircle([ballCircle.x, ballCircle.y], ballCircle.radius, ballColor);
}

function launchBall(){
	if(!isLaunched){
		ballVelocity.y = 0.005;
		isLaunched = true;
		messagesText.innerHTML = '';
	}
}